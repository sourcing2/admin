import { Button } from '@paljs/ui/Button';
import { InputGroup } from '@paljs/ui/Input';
import { useRouter } from 'next/router';
import Auth from 'components/Auth';
import React, { useState } from 'react';
import Layout from 'Layouts';
import { login } from '../../../api';
import { toast } from "react-toastify";
export default function Login() {

  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  
  const router = useRouter();

  const handleLogin = async(e: { preventDefault: () => void; }) => {
    e.preventDefault()
    const values = {
      email,
      password
    };
    await login(values).then(function (res: { status: number; data: { token: string; error: React.SetStateAction<string>; }; }) {
      if (res && res.status == 200) {
        console.log(res.data.token);
        localStorage.setItem("token",res.data.token);
        
        router.push({
          pathname: '/'
        });
        
      } else {
        console.log(res);
        
        toast.error(res.data.error);
        
      }
    })
  } 

  return (
    <Layout title="Login">
      <Auth title="Login" subTitle="Hello! Login with your email">
        <form onSubmit={handleLogin}>
          <InputGroup fullWidth>
            <input type="email" placeholder="Email Address" onChange={e => setEmail(e.target.value)} value={email} required/>
          </InputGroup>
          <InputGroup fullWidth>
            <input type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} value={password} required/>
          </InputGroup>
          {/* <Group>
            <Checkbox checked onChange={onCheckbox}>
              Remember me
            </Checkbox>
            <Link href="/auth/request-password">
              <a>Forgot Password?</a>
            </Link>
          </Group> */}
          <Button status="Success" type="submit" shape="SemiRound" fullWidth>
            Login
          </Button>
        </form>
        
        {/* <Socials /> */}
        {/* <p>
          Don&apos;t have account?{' '}
          <Link href="/auth/register">
            <a>Register</a>
          </Link>
        </p> */}
      </Auth>
    </Layout>
  );
}
