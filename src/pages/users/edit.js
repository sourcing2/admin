import { Button } from '@paljs/ui/Button';
import { Card, CardBody } from '@paljs/ui/Card';
import Row from '@paljs/ui/Row';
import Col from '@paljs/ui/Col';
import { InputGroup } from '@paljs/ui/Input';
import React, { useEffect, useState } from 'react';
import Layout from 'Layouts';
import { getUserbyid, UpdateUser } from "../../../api";
import { toast } from "react-toastify";
import styled from 'styled-components';
import { FormControl, Select, MenuItem } from "@material-ui/core";
import { useRouter } from "next/router";
import { Formik } from "formik";

import * as Yup from "yup";

export const SelectStyled = styled(Select)`
  margin-bottom: 1rem;
`;
const selectstyle = { marginTop: '10px' };
const UserEdit = () => {
    const router = useRouter()
    let [data, setData] = useState(null);
    let [Userid, setUserid] = useState("all");

    const [loading, setLoading] = useState(false);
    const validationSchema = Yup.object().shape({
        company: Yup.string()
            .required('Required'),
        email: Yup.string().email('Invalid email address').required('Required'),
        maximum_search_allowed: Yup.string().required('Required'),
    });

    const statusArray = [
        { value: "Active", label: "Active" },
        { value: "InActive", label: "InActive" }
    ];
    
    useEffect(() => {
        if (router && router.query && router.query.id && (router.query.id != undefined || router.query.id != '')) {
            const { id } = router.query.id;
            setUserid(router.query.id);
            Userdata(router.query.id);
        }
    }, [router.query]);
    
    const Userdata = async (id) => {
        
        await getUserbyid(id)
        .then(function(res) {
            if(res && res.status == 200) {
                setData(res.data.user);
            } else {
                toast.error(res.message);
            }
        })
    }

    return (
        <div>
            {
            !loading ?
            (
            <>
                {
                    data ? (
                        <Layout className="create">
                            <Row>
                                <Col breakPoint={{ xs: 12, md: 12 }}>
                                    <Card>
                                        <header>Update User</header>
                                        <CardBody>
                                            <Row>
                                                <Col breakPoint={{ xs: 12, md: 12 }}>
                                                    <Formik
                                                        enableReinitialize="true"
                                                        initialValues={{
                                                            company: data.company,
                                                            email: data.email,
                                                            maximum_search_allowed: data.maximum_search_allowed,
                                                        }}
                                                        validationSchema={validationSchema}
                                                        onSubmit={async (values) => {
                                                            setLoading(true);
                                                            await UpdateUser(values,Userid)
                                                            .then(function(res) {
                                                                if(res && res.status == 200) {
                                                                    
                                                                    toast.success("User Updated Successfully");
                                                                    router.push({
                                                                        pathname: '/users'
                                                                    });
                                                                } else {
                                                                    setLoading(false);
                                                                    toast.error(res.message);
                                                                    
                                                                }
                                                            })
                                                        }}
                                                    >
                                                        {({ handleSubmit, handleChange, values, setFieldValue, errors, touched }) => (
                                                            <div>
                                                                <Row>
                                                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                                                        <div style={{ marginTop: '10px' }}>
                                                                            <label htmlFor="name">Company / Name <span className="required">*</span></label>
                                                                            <InputGroup fullWidth>
                                                                                <input id="name" name="company" type="text" onChange={handleChange} value={values.company} className={touched.company && errors.company ? "error" : null} />

                                                                            </InputGroup>
                                                                            {touched.company && errors.company ? (
                                                                                <div className="error-message">{errors.company}</div>
                                                                            ) : null}
                                                                        </div>
                                                                    </Col>
                                                                    
                                                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                                                        <div style={{ marginTop: '10px' }}>
                                                                            <label htmlFor="email">Email Id <span className="required">*</span></label>
                                                                            <InputGroup fullWidth>
                                                                                <input id="email" name="email" type="text" onChange={handleChange} value={values.email} className={touched.email && errors.email ? "error" : null} />
                                                                            </InputGroup>
                                                                            {touched.email && errors.email ? (
                                                                                <div className="error-message">{errors.email}</div>
                                                                            ) : null}
                                                                        </div>
                                                                    </Col>
                                                                    
                                                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                                                        <div style={{ marginTop: '10px' }}>
                                                                            <label htmlFor="maximum_search_allowed">Maximum Search Allowed <span className="required">*</span></label>
                                                                            <InputGroup fullWidth>
                                                                                <input id="maximum_search_allowed" name="maximum_search_allowed" type="text" onChange={handleChange} value={values.maximum_search_allowed} className={touched.maximum_search_allowed && errors.maximum_search_allowed ? "error" : null} />
                                                                            </InputGroup>
                                                                            {touched.maximum_search_allowed && errors.maximum_search_allowed ? (
                                                                                <div className="error-message">{errors.maximum_search_allowed}</div>
                                                                            ) : null}
                                                                        </div>
                                                                    </Col>
                                                                    
                                                                </Row>
                                                                <Row>
                                                                    <Col breakPoint={{ xs: 12, md: 4 }}></Col>
                                                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                                                        <div style={{ marginTop: '10px' }}>
                                                                            <Button status="Success" type="button" shape="SemiRound" onClick={handleSubmit}>
                                                                                Submit
                                                                            </Button>
                                                                        </div>
                                                                    </Col>
                                                                </Row>
                                                            </div>
                                                        )}
                                                    </Formik>
                                                </Col>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Layout>
                    ) : (null)
                }
            </>
            ) : (
            null
            )
        }
        </div>

    );
};



export default UserEdit;
